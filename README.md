El teclado táctil de Windows 8 incluye varias funciones para que la escritura sea más rápida y precisa. Para hacer más eficiente la escritura, Windows sugiere palabras en la pantalla mientras se escribe, permitiéndole elegir el trabajo sugerido con un solo clic.

Si utilizas Windows 8 en un ordenador de sobremesa, portátil o netbook, puedes probar el nuevo teclado táctil. Aunque el teclado táctil sólo se puede utilizar en una máquina con pantalla táctil, puedes activarlo en tu PC o portátil habitual siguiendo las instrucciones que aparecen a continuación:

Paso 1: Cambie al modo de escritorio. Haga clic con el botón derecho del ratón en la barra de tareas y seleccione Propiedades .

Paso 2: En el cuadro de diálogo Propiedades de la barra de tareas, vaya a la pestaña Barras de herramientas.

Paso 3: Marque la opción Teclado Táctil y haga clic en el botón Aplicar para que aparezca un pequeño icono de teclado en la barra de tareas.

Paso 4: Haga clic en el icono del teclado en la barra de tareas para empezar a utilizar el teclado táctil.

https://clomatica.com/habilitar-o-deshabilitar-la-pantalla-tactil-en-windows-8-1/